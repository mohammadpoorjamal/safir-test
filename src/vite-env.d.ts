/// <reference types="vite/client" />

// IntelliSense for TypeScript 
interface ImportMetaEnv {
    readonly VITE_APP_TITLE: string
    readonly VITE_APP_API_URL: string
    readonly VITE_APP_API_TIMEOUT: string
}

interface ImportMeta {
    readonly env: ImportMetaEnv
}