import { teal } from "@mui/material/colors";
import grey from "@mui/material/colors/grey";

export const lightPalette = {
  // palette values for light mode
  primary: {
    main: "#4385F6",
    100: "#4385F6",
    75: "#959DF7",
    50: "#B8BDF9",
    25: "#CFDBE3",
    20: "#0F4C7533",
    back:"#CFDBE3"
  },
  divider: "#EBEBEB",

  background: {
    main: "#E8F0F2",
    default: "#F5F5F5",
    1: "#E3EDFE",
    2: "#F5F8FE",
    3: "#FFFFFF",
  },
  secondary: {
    // main: "#FBBB22",
    main: "#F8BD07",
    100: "#F8BD07",
    75: "#FFDB85",
    50: "#FFF4D2",
    25: "#FFE7AD",
  },
  error: {
    main: "#E74C3C",
    100: "#E74C3C",
    75: "#ED796D",
    50: "#F3A59D",
    light: "#E74C3C"
  },
  info: {
    main: "#1F74E2",
  },
  gray: {
    main: "#1B262C",
    100: "#1B262C",
    75: "#545C61",
    50: "#8D9395",
    25: "#C6C9CA",
    back:"#EBEBEB",
    light: "#EBEBEB"
  },
  action: {
    // hover: "rgba(0,0,0,0.2)"
  },
  text: {
    primary: "#1B262C",
    secondary: "#1B262C",
    100: "#1B262C",
    75: "#545C61",
    50: "#8D9395",
    25: "#C6C9CA",
  },

  // text: {
  //   primary: teal[50],
  //   secondary: grey[300],
  // },
};
