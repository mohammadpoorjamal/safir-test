import { OverridesStyleRules } from "@mui/material/styles/overrides";
import { Theme } from "@mui/material/styles";
import {ButtonProps, ButtonClasses} from "@mui/material";

type TMuiButton =
  | {
      defaultProps?: Partial<ButtonProps<"button", {}>> | undefined;
      styleOverrides?:
        | Partial<
            OverridesStyleRules<
              keyof ButtonClasses,
              "MuiButton",
              Omit<Theme, "components">
            >
          >
        | undefined;
      variants?: [] | undefined;
    }
  | undefined;

export const MuiButton: TMuiButton = {
  defaultProps: {disableElevation: true, variant: 'contained'},
  styleOverrides: {
    root: ({ownerState , theme}) => ({
      borderRadius: 100,
      padding: "10px 32px",
      fontSize: 14,
      fontWeight: 700,
      minWidth: ownerState.size === "large" ? 220 : "auto"
    }),
  },
}
