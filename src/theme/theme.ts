import "./palette/palette.d.ts";

import { createTheme } from "@mui/material";
import { MuiCssBaseline } from "./mui-css-baseline";
import { lightPalette } from "~/theme/palette/light/lightPalette";
import { typography } from "./Typography/index";
import { MuiButton } from "~/theme/Button/MuiButton";


const customTheme = () =>
  createTheme({
    direction: 'rtl',
    palette: lightPalette,
    spacing: 4,
    typography,
    breakpoints: {
      values: {
        xs: 0,
        sm: 600,
        md: 900,
        lg: 1200,
        xl: 1536,
      },
    },
    components: {
      MuiCssBaseline,
      MuiButton,
       MuiTypography: {
        styleOverrides: {
          root: ({ theme, ownerState }) => ({
            ...(theme.palette.mode === "dark" && {
              color: "#F2F2F2",
            }),
            lineHeight: 1.5
          })
        }
      }
    },
  });

export default customTheme;
