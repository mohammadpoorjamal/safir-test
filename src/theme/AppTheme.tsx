import React, { FC, ReactNode, useMemo } from "react";
import { ThemeProvider } from "@mui/material";
import createCache from "@emotion/cache";
import { prefixer } from "stylis";
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import customTheme from "./theme";
import { useDispatch } from "react-redux";

const cacheRtl = createCache({
  key: "rtl",
  stylisPlugins: [prefixer, rtlPlugin],
});

interface AppThemeProps {
  children: ReactNode;
}

const AppTheme: FC<AppThemeProps> = ({ children }) => {
  const theme = React.useMemo(() => customTheme(), []);

  const cacheValue = useMemo(() => cacheRtl, []);

  return (
    <CacheProvider value={cacheValue}>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </CacheProvider>
  );
};

export default AppTheme;
