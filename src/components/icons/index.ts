export { default as Facebook } from "./Facebook";
export { default as Instagram } from "./Instagram";
export { default as Linkedin } from "./Linkedin";
export { default as Message } from "./Message";
export { default as Vector } from "./Vector";
export { default as WhatsApp } from "./WhatsApp";
