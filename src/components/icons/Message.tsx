// Auto-generated file created by svgr-cli source svg-template.js
// Run yarn icons:create to update
// Do not edit
import * as React from "react";
import type {FC, SVGProps} from "react";
interface Props extends SVGProps<any> {
  primarycolor?: string;
}
const SvgMessage: FC = (props: Props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={22}
    height={22}
    fill="none"
    viewBox="0 0 22 16"
    {...props}
  >
    <path
      fill={props?.primarycolor || "#4385F6"}
      d="M19.323.666C13.97.66 8.617.666 3.263.663c-.32.004-.644-.02-.959.05C1.726.842 1.2 1.225.931 1.756c-.16.294-.243.627-.244.96v10.571c.003.33-.023.668.073.99.217.847 1.029 1.497 1.905 1.512h15.942c.45-.01.918.05 1.349-.119a2.065 2.065 0 0 0 1.356-1.866c.002-3.696 0-7.392 0-11.087a2.053 2.053 0 0 0-.744-1.576 2.073 2.073 0 0 0-1.245-.474Zm-.367 1.37c-2.183 1.67-4.358 3.351-6.538 5.025a2.35 2.35 0 0 1-1.07.454 2.345 2.345 0 0 1-1.765-.454c-2.18-1.674-4.356-3.354-6.539-5.025 5.304.003 10.608.004 15.912 0Zm.98.982c.002 3.567.001 7.134.001 10.7a.69.69 0 0 1-.644.694c-5.515.002-11.03 0-16.545.002-.37.011-.699-.328-.686-.697l.001-10.699C4.29 4.731 6.516 6.442 8.741 8.155c.643.493 1.451.768 2.261.76a3.688 3.688 0 0 0 2.258-.76l6.677-5.137Z"
    />
  </svg>
);
export default SvgMessage;
