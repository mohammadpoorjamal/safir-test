// Auto-generated file created by svgr-cli source svg-template.js
// Run yarn icons:create to update
// Do not edit
import * as React from "react";
import type { SVGProps } from "react";
interface Props extends SVGProps<any> {
  primarycolor?: string;
}
const SvgFacebook = (props: Props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={25}
    fill="none"
    viewBox="0 0 24 24"
    {...props}
  >
    <path
      fill={props?.primarycolor || "#4385F6"}
      fillRule="evenodd"
      d="M2 6.25c0-1.105.421-2.165 1.172-2.946A3.92 3.92 0 0 1 6 2.084h12a3.92 3.92 0 0 1 2.828 1.22A4.256 4.256 0 0 1 22 6.25v12.5a4.256 4.256 0 0 1-1.172 2.946A3.92 3.92 0 0 1 18 22.916H6a3.92 3.92 0 0 1-2.828-1.22A4.256 4.256 0 0 1 2 18.75V6.25Zm4-2.083c-.53 0-1.04.22-1.414.61C4.21 5.167 4 5.697 4 6.25v12.5c0 .552.21 1.082.586 1.473.375.39.884.61 1.414.61h6v-7.291h-1a.98.98 0 0 1-.707-.306A1.064 1.064 0 0 1 10 12.5c0-.276.105-.541.293-.737a.98.98 0 0 1 .707-.305h1V9.896c0-.967.369-1.895 1.025-2.578A3.43 3.43 0 0 1 15.5 6.25h.6c.265 0 .52.11.707.305.188.195.293.46.293.737 0 .276-.105.54-.293.736a.98.98 0 0 1-.707.305h-.6c-.197 0-.392.04-.574.12a1.498 1.498 0 0 0-.487.338 1.57 1.57 0 0 0-.325.507c-.075.19-.114.393-.114.598v1.562h2.1c.265 0 .52.11.707.305.188.196.293.46.293.737 0 .276-.105.541-.293.736a.98.98 0 0 1-.707.306H14v7.291h4c.53 0 1.04-.22 1.414-.61.375-.39.586-.92.586-1.473V6.25c0-.553-.21-1.083-.586-1.473A1.96 1.96 0 0 0 18 4.167H6Z"
      clipRule="evenodd"
    />
  </svg>
);
export default SvgFacebook;
