import React, { FC } from "react";
import { Button, ButtonProps } from "@mui/material";
import Box from "@mui/material/Box";
import { ArrowLeft } from "iconsax-react";

interface Props extends ButtonProps {
  children: React.ReactNode;
}

const ArrowButton: FC<Props> = ({ children, color, ...rest }) => {
  return (
    <Box position={"relative"} width={'fit-content'} sx={{ "&:hover .arrow-container": {  } }}>
      <Button color={color} {...rest} sx={{color: "white"}}>{children}</Button>
      <Box
        className={"arrow-container"}
        sx={{transition: "all 0.3s"}}
        display={"flex"}
        p={2}
        borderRadius={99}
        bgcolor={color ? color + ".100" : "primary.100"}
        border={"3px solid white"}
        position={"absolute"}
        top={3}
        right={-20}
      >
        <ArrowLeft color={"white"} size={18} />
      </Box>
    </Box>
  );
};

export default ArrowButton;
