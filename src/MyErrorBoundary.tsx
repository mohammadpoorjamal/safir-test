import React, { FC, ReactNode } from "react";

interface IHttpInterceptor {
  children?: ReactNode;
}

const MyErrorBoundary : React.FC<IHttpInterceptor> = ({ children }) => {

  return <>{children}</>

}

export default MyErrorBoundary

