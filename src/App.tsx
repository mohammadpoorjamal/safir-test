import { RouterProvider } from "react-router-dom";
import { router } from "./router";
import "./style/style.css";
import Box from "@mui/material/Box";

function App() {
  return (
      <Box dir={'rtl'}>
        <RouterProvider router={router} />
      </Box>
  );
}

export default App;
