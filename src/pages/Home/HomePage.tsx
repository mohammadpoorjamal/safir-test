import React, { FC } from "react";
import Box from "@mui/material/Box";
import Header from "~/pages/Home/components/Header";
import Heading from "~/pages/Home/components/heading/Heading";

const HomePage: FC = () => {
  return <Box>
    <Header />
    <Heading />
  </Box>;
};

export default HomePage;
