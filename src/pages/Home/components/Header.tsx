import React, {FC} from 'react';
import Box from "@mui/material/Box";
import Instagram from "~/components/icons/Instagram";
import Linkedin from "~/components/icons/Linkedin";
import WhatsApp from "~/components/icons/WhatsApp";
import Facebook from "~/components/icons/Facebook";
import {Container, Typography} from "@mui/material";
import {useNavigate} from "react-router-dom";
import Message from "~/components/icons/Message";
import SvgVector from "~/components/icons/Vector";

const Header: FC = () => {
  const  navigate = useNavigate()

  const handleContact = () => {
    window.open("")
  }

  return (
    <Container component={'header'} sx={{display: 'flex', my: 5, justifyContent: "space-between"}} maxWidth={"lg"}>
      <Box display={'flex'} gap={5} >
        <Instagram style={{cursor: "pointer"}} onClick={handleContact}  />
        <Linkedin style={{cursor: "pointer"}} onClick={handleContact} />
        <WhatsApp style={{cursor: "pointer"}} onClick={handleContact} />
        <Facebook  style={{cursor: "pointer"}} onClick={handleContact}/>
      </Box>

      <Box display={'flex'} gap={2}>
        <Box display={'flex'} gap={2} alignItems={'center'}>
          <Typography>info@Academy.com</Typography>
          <Message />
        </Box>
        <Box display={'flex'} gap={2} alignItems={'center'}>
          <Typography>021-22874522</Typography>
          <SvgVector />
        </Box>
      </Box>
    </Container>
  );
};

export default Header;
