import React, { FC } from "react";
import Box from "@mui/material/Box";
import { Container, Stack } from "@mui/material";
import MainHeader from "~/pages/Home/components/heading/components/MainHeader";
import Intro from "~/pages/Home/components/heading/components/Intro";
import LanguageScrollbar from "~/pages/Home/components/heading/components/LanguageScrollbar";

const Heading: FC = () => {
  return (
    <Container maxWidth={"xl"}>
      <Stack borderRadius={12.5} px={{xs: 4, lg: 25}} py={12.5} bgcolor={"background.1"}>
        <Box mb={25}>
          <MainHeader />
        </Box>

        <Box mb={40}>
        <Intro />
        </Box>
        <LanguageScrollbar />
      </Stack>
    </Container>
  );
};

export default Heading;
