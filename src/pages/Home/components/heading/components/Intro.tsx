import React from "react";
import { Grid, Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import ArrowButton from "~/components/common/Button/ArrowButton";

import IntroImage from "~/assets/images/intro-image.png";

const Intro = () => {
  return (
    <Grid container>
      <Grid item xs={12} lg={6}>
        <Stack gap={9}>
          <Box>
            <Typography fontSize={30} fontWeight={900} lineHeight={1.5}>
              آموزش آنلاین زبان
            </Typography>
            <Typography fontSize={30} fontWeight={900} lineHeight={1.5}>
              همیشه و همه‌جا ، با استادی که دوست داری
            </Typography>
          </Box>

          <Typography fontSize={20} fontWeight={500}>
            همسوترین استاد با اهداف و نیازهایت را از بین صدها استاد ارزیابی‌شده،
            انتخاب کن و در زمان‌های دلخواه، در کلاس زبان آنلاین شرکت کن
          </Typography>

          <ArrowButton color={"secondary"}>همین حالا شروع کن</ArrowButton>
        </Stack>
      </Grid>

      <Grid item xs={12} lg={6}>
        <Box position={"relative"}>
          <Box
            position={"absolute"}
            top={0}

            width={315}
            height={315}
            bgcolor={'white'}
            borderRadius={99}
            border={"20px solid #c3daef"}
            zIndex={1}
          />

          <Box zIndex={2} position={'relative'}>
          <img src={IntroImage} style={{zIndex: 2}}  alt={''}/>
          </Box>


        </Box>
      </Grid>
    </Grid>
  );
};

export default Intro;
