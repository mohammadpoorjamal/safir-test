import React, { FC, useCallback, useRef } from "react";
import Box from "@mui/material/Box";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";

import flage1 from "~/assets/images/countries/Flags.png";
import flage2 from "~/assets/images/countries/Flags-1.png";
import flage3 from "~/assets/images/countries/Flags-2.png";
import flage4 from "~/assets/images/countries/Flags-3.png";
import flage5 from "~/assets/images/countries/Flags-4.png";
import flage6 from "~/assets/images/countries/Flags-5.png";
import flage7 from "~/assets/images/countries/Flags-6.png";
import flage8 from "~/assets/images/countries/Flags-7.png";
import { Button, Stack, Typography, useTheme } from "@mui/material";
import { A11y, Navigation } from "swiper";
import { ArrowLeft, ArrowRight } from "iconsax-react";

const LanguageScrollbar: FC = () => {
  const countries = [
    {
      label: "انگلیسی",
      image: flage1,
    },
    {
      label: "فرانسوی",
      image: flage2,
    },
    {
      label: "آلمانی",
      image: flage3,
    },
    {
      label: "ترکی",
      image: flage4,
    },
    {
      label: "اسپانیایی",
      image: flage5,
    },
    {
      label: "کره ای",
      image: flage6,
    },
    {
      label: "ایتالیایی",
      image: flage7,
    },
    {
      label: "چینی",
      image: flage8,
    },
  ];

  const sliderRef = useRef<any>(null);

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current?.swiper?.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current?.swiper?.slideNext();
  }, []);

  const theme = useTheme();


  return (
    <Box
      px={15}
      py={7}
      pt={9}
      borderRadius={99}
      bgcolor={"white"}
      position={"relative"}
    >
      <Swiper
        style={{ position: "relative" }}
        ref={sliderRef}
        modules={[Navigation, A11y]}
        spaceBetween={40}
        slidesPerView={8}
        onSlideChange={() => console.log("slide change")}
        onSwiper={(swiper) => console.log(swiper)}
        loop={true}
      >
        {countries.map((country) => (
          <SwiperSlide>
            <Stack justifyContent={"center"} alignItems={"center"} gap={2}>
              <img width={53} height={37} src={country.image} alt={"flag"} />
              <Typography fontWeight={500}>{country.label}</Typography>
            </Stack>
          </SwiperSlide>
        ))}
      </Swiper>

      <Box

        display={"flex"}
        alignItems={"center"}
        justifyContent={"center"}
        position={"absolute"}
        top={"50%"}
        right={-20}
        sx={{ transform: "translateY(-50%)", cursor: "pointer" }}
        zIndex={10}
        width={50}
        height={50}
        borderRadius={100}
        border={"1px solid"}
        borderColor={"primary.100"}
        onClick={handleNext}
      >
        <ArrowLeft color={theme.palette.primary.main} />
      </Box>
      <Box

        display={"flex"}
        alignItems={"center"}
        justifyContent={"center"}
        position={"absolute"}
        top={"50%"}
        left={-20}
        sx={{ transform: "translateY(-50%)", cursor: "pointer" }}
        zIndex={10}
        width={50}
        height={50}
        borderRadius={100}
        border={"1px solid"}
        borderColor={"primary.100"}
        onClick={handlePrev}
      >
        <ArrowRight color={theme.palette.primary.main} />
      </Box>
    </Box>
  );
};

export default LanguageScrollbar;
