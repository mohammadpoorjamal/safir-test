import React, {FC} from 'react';
import LogoImage from '~/assets/images/Logo 1.png'
import Box from "@mui/material/Box";
import {Typography} from "@mui/material";

const Logo: FC = () => {
  return (
    <Box display={'flex'} gap={2}>
      <img src={LogoImage} alt={"logo"} width={50} height={50} />
      <Box>
        <Typography fontWeight={900} fontSize={18}>موسسـه سفیــر</Typography>
        <Typography>آموزش زبان‌های خارجی</Typography>
      </Box>
    </Box>
  );
};

export default Logo;
