import React, { FC } from "react";
import Box from "@mui/material/Box";
import Logo from "~/pages/Home/components/heading/components/Logo";
import { Link } from "react-router-dom";
import {Typography} from "@mui/material";
import ArrowButton from "~/components/common/Button/ArrowButton";

const MainHeader: FC = () => {
  const links = [
    "دوره‌های خودآموز",
    "آیلتس",
    "سطح بندی",
    "کلاس‌های خصوصی",
    "همکاری با ما",
    "بلاگ",
  ];

  return (
    <Box display={"flex"} alignItems={'center'} justifyContent={'space-between'}>
      <Logo />

      <Box display={{xs: "none", lg: "flex"}} gap={12.5}>
        {links.map((link) => (
          <Link to={link}>
            <Typography fontSize={14}>
              {link}
            </Typography>
          </Link>
        ))}
      </Box>

      <ArrowButton size={"large"}>
        ورود و ثبت نام
      </ArrowButton>
    </Box>
  );
};

export default MainHeader;
