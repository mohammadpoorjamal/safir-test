import { createSlice } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";
import { RootState } from "~/store/store";

export interface CommonState {}

const initialState: CommonState = {};

export const CommonSlice = createSlice({
  name: "common",
  initialState,
  reducers: {},
});

export const useCommon = () => useSelector((state: RootState) => state.common);
export const {} = CommonSlice.actions;
export default CommonSlice.reducer;
