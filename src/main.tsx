import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import {CssBaseline} from "@mui/material";

import {Provider} from "react-redux";
import {store} from "~/store/store";
import AppTheme from "~/theme/AppTheme";
import {} from '@mui/material'


ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <Provider store={store}>
    <AppTheme>
      <App/>
      <CssBaseline/>
    </AppTheme>
  </Provider>
);
