export const clearObject = (obj: {}) => {
  const res = Object.entries(obj).filter((entry) => entry[1]);
  return Object.fromEntries(res);
};
