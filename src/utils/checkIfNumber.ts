export const checkIfNumber = (e: any) => {
  const charLength = e.target.value.length;
  return !charLength || !isNaN(e.target.value.at(-1))
}