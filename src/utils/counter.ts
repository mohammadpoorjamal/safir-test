export type TTimer = {
  seconds: number | string;
  minutes: number | string;
};

export const startTimer = (
  duration: any,
  setTimer: ({ seconds, minutes }: TTimer) => void
) => {
  let start = Date.now(),
    diff,
    minutes,
    seconds;

  function timer() {
    // get the number of seconds that have elapsed since
    // startTimer() was called
    diff = duration - (((Date.now() - start) / 1000) | 0);

    // does the same job as parseInt truncates the float
    minutes = (diff / 60) | 0;
    seconds = diff % 60 | 0;

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    setTimer({ seconds, minutes });

    if (diff <= 0) {
      clearInterval(timerInterval)
    }
  }
  // we don't want to wait a full second before the timer starts
  timer();
  const timerInterval = setInterval(timer, 1000);

};
