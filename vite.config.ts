/* eslint-disable import/no-extraneous-dependencies */
/// <reference types="vitest" />
/// <reference types="vite/client" />
/// <reference types="vite-plugin-svgr/client" />
import checker from "vite-plugin-checker";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import svgr from "vite-plugin-svgr";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svgr({
      exportAsDefault: false,
      svgrOptions: {
        expandProps: "end",
        replaceAttrValues: {
          "#727CF4": "{props?.primarycolor || '#727CF4'}",
          "#CFDBE3": "{props?.secondarycolor || '#CFDBE3'}"
        },
      },
    }),
    react(),
    checker({
      typescript: true,
    }),

  ],
  base: "/",
  server: {
    host: true,
    port: 3000,
  },
  preview: {
    host: true,
    port: 4000,
  },
  resolve: {
    alias: [
      { find: "~", replacement: path.resolve(__dirname, "src") },
      {
        find: "icons",
        replacement: path.resolve(__dirname, "src/assets/icons"),
      },
    ],
  },
});
